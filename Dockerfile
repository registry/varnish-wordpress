FROM debian:jessie

# Install Varnish from source, so that Varnish modules can be compiled and installed.
ENV VARNISH_VERSION=4.1.0
ENV VARNISH_SHA256SUM=4a6ea08e30b62fbf25f884a65f0d8af42e9cc9d25bf70f45ae4417c4f1c99017

RUN \
  useradd -r -s /bin/false varnishd \
  && apt-get update && apt-get install -y --no-install-recommends \
    automake \
    build-essential \
    ca-certificates \
    curl \
    libedit-dev \
    libjemalloc-dev \
    libncurses-dev \
    libpcre3-dev \
    libtool \
    pkg-config \
    python-docutils \
  && mkdir -p /usr/local/src && \
  cd /usr/local/src && \
  curl -sfLO https://repo.varnish-cache.org/source/varnish-$VARNISH_VERSION.tar.gz && \
  echo "${VARNISH_SHA256SUM} varnish-$VARNISH_VERSION.tar.gz" | sha256sum -c - && \
  tar -xzf varnish-$VARNISH_VERSION.tar.gz && \
  cd varnish-$VARNISH_VERSION && \
  ./autogen.sh && \
  ./configure && \
  make install && \
  rm ../varnish-$VARNISH_VERSION.tar.gz \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY ./default.vcl /etc/varnish/default.vcl
COPY ./conf /etc/varnish/conf
COPY ./lib /etc/varnish/lib
COPY ./start-varnishd.sh /usr/local/bin/start-varnishd

RUN chmod +x /usr/local/bin/start-varnishd \
  && echo "123456" > /etc/varnish/secret \
  && find /etc/varnish -type f -exec chmod 644 {} +

ENV VARNISH_PORT 6081
ENV VARNISH_MEMORY 128m
ENV VARNISH_DAEMON_OPTS="-T 127.0.0.1:6082 -S /etc/varnish/secret"

ENV LD_LIBRARY_PATH /usr/local/lib

EXPOSE 6081
CMD ["/usr/local/bin/start-varnishd"]
